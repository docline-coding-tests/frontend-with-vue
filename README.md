# DOCLINE FRONTEND VUE TEST

## INTRODUCTION

We are going to create a simple website with Vue and the [Punk API](https://punkapi.com/)!
Our client asks for a site that can display a list of items, fetched from an external API. Also, the site should offer to the user the possibility to get more detailed info of each item.
Simple, right? Happy coding!

## THE EXERCISE

* Showcase beers in a grid:
  * Feel free to experiment with columns and gutters
  * Feel free to experiment with pagination and/or lazy loading
  * Be aware about performance and best practices working with APIs
* Filtering:
  * Filter by name
  * Filter by date of elaboration
  * Filter by alcoholic/non alcoholic
* Each card will show:
  * Image
  * Title
  * Short description
  * Excerpt
  * "Read more" button
  * A distinctive label, if non-alcoholic
* The Read more button will trigger a modal with the full specs of the selected item

## REQUIREMENTS

* Deliver the exercise in a VCS repository, with all the commit history available
* We prefer the use of Vue.js over other any actual JS framework. Feel free to use Nuxt if you want to add SSR rendering
* Take your time to complete a README with all relevant details
* Global state management. Even though it's not essential for this kind of website, a global state management will be useful, especially for the bonuses. We use Vuex, obviously :)
* A *get random beer* button. We need a button to fetch a random beer when clicked. Feed the beer modal with this data

If you got to this point, congratulations! It's all we ask for in this process, YAY!
However, if you feel in the mood, please take a look at our bonuses and earn some extra points :green_heart:

:point_down:

## BONUSES

In addition to the base requirements, feel free to implement any of this features.

* Nowadays, we are using **Vuetify** to speed up our frontend development. Feel free to use it, or use another UI framework of your choice. Work smart not hard (most of the time). 1 point
* Use CSS custom properties. 1 point
* Show the results count when filtering the items. 1 point
* Use BEM or any other CSS methodology. 2 points
* A (minimum) responsive adaptation of the elements and layout. 2 points
* Optimize performance in any way you can think of. 2 points
* Dark mode, the new hype. 2 points
* Some unit testing. We do not ask for any specific coverage, feel free to cover the parts of your consideration. 3 points
* Some end2end testing. We do not ask for any specific coverage, feel free to cover the parts of your consideration. 4 points
* Surprise us! Add any feature you like, but think it twice, avoid non user relevant ones and keep it simple. _n_ points

## EVALUATION

The evaluation will focus on:

* UI and element distribution
* Project organization and clean code patterns
* Components: structure and reusability
* Use of the asynchrony and the proper management of the API
* Aesthetic taste and care for visual aspects

## DEMO ASSETS

We provide a design reference for the site, with basic layout and some components. Feel free to clone, modify or entirely change it.
We used Vuetify components for this demo, don't worry if you use another UI kit.

The font family used in this demo is [Inconsolata](https://fonts.google.com/specimen/Inconsolata)

Colors:
  
```
:root {
  --color-base: #0a0a0a;
  --color-primary: #00afdb;
  --color-secondary: #009688;
  --color-white: white;
  --color-danger: #de2f2f;
  --color-soft: #dadada;
  --color-dark: #31264e;
}
```


### Default

![Punk API 1](assets/PunkApi_1_main.png)


### Filtering

![Punk API 2](assets/PunkApi_2_search.png)


### Hovering

![Punk API 3](assets/PunkApi_3_date-filter_hover.png)

### Video demo

![Punk API Video demo](assets/video_ref.mp4)
